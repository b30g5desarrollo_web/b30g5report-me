// importar modulo de MongoDB => mongoose
const mongoose = require('mongoose');

// Importar variables de entorno
require('dotenv').config({path: 'var.env'})

const conexionDB = async() => {
    try {

        await mongoose.connect(process.env.URL_MONGODB, {});
        
        console.log("conexion establecida con MongoDB atlas desde el archivo db.js");
   
    } catch (error) {
        console.log("error de conexion base de datos");
        console.log(error);
        process.exit(1);
    }
}

// exportar como modulo para utilizar en otros archivos 
module.exports = conexionDB;
